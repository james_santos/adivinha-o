/*
Construa um algoritmo que seja capaz de concluir qual dentre os seguintes
animais foi escolhido, atrav�s de perguntas e respostas. Animais poss�veis:
Leao, Cavalo, Homem, Macaco, Morcego, Baleia, Avestruz,
Pinguim, Pato, Aguia, Tartaruga, Crocodilo e Cobra.

Autor: Jameson Stracci


Mamifero(
	Quadrupede(
		Carnivero(Leao)
		Herbivoro(Cavalo)
	)
	B�pede(
		Onivoro(Homem)
		Frutivoro(Macaco)
	)	
	Voadores(Morcego)
	Aqu�ticos(Baleia)
)
Aves(
	Nao-voadoras(
		Tropicais(Avestruz)
		Polares(Pinguim)
	Nadadoras(Pato)		
	De Rapina(Aguia)
	
)
Repteis(
	Com Casco(Tartaruga)
	Carn�voros(Crocodilo)
	Sem Patas(Cobra)
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <locale.h>
int main(){
	setlocale(LC_ALL,"Portuguese");
	char opt;
	
	
	printf("###########################################\n");
	printf("# Escolha Um dos animais que vou advinhar #\n");
	printf("# Leao, Cavalo, Homem, Macaco, Morcego,   #\n");
	printf("# Baleia, Avestruz, Pinguim, Pato, Aguia, #\n");
	printf("# Tartaruga, Crocodilo e Cobra            #\n");
	printf("###########################################\n");
	printf("Digite S para (SIM) ou N para (Nao). Seu animal � Mam�fero?: \n");
	scanf("%c", &opt);
	
	// Cadeia de SELE��O dos MAM�FEROS
	if (opt == 's' || opt == 'S'){
		fflush(stdin);
		printf("Eh Quadrupede?: ");
		scanf("%c", &opt);
		//Cadeia de SELE��O dos QUADR�PEDES.
		if (opt == 's' || opt == 'S'){
			fflush(stdin);
			printf("Eh Carnivoro?: ");
			scanf("%c", &opt);
			if (opt == 's' || opt == 'S'){
				fflush(stdin);
				printf("Eh o LEAO!!!");
			}else if (opt == 'n' || opt == 'N'){
				fflush(stdin);
				printf("Eh Herbivoro?: ");
				scanf("%c",&opt);
				if (opt == 's' || opt == 'S'){
					printf("Eh o CAVALO!!");
				}
			}
		}else if (opt == 'n' || opt == 'N'){
			fflush(stdin);
			printf("Eh Bipede?: ");
			scanf("%c", &opt);
			//Cadeia de SELE��O dos B�PEDES.
			if (opt == 's' || opt == 'S'){
				fflush(stdin);
				printf("Eh Onivoro?: ");
				scanf("%c", &opt);
				if (opt == 's' || opt == 'S'){
					fflush(stdin);
					printf("Eh o HOMEM!!");
				}else if (opt == 'n' || opt == 'N'){
					fflush(stdin);
					printf("Eh Frutivoro?:");
					scanf("%c", &opt);
					if (opt == 's' || opt == 'S'){
						fflush(stdin);
						printf("Eh o MACACO!!");
					}
				}
			}else if (opt == 'n' || opt == 'N'){
				fflush(stdin);
				printf("Eh Voador?: ");
				scanf("%c", &opt);
				if (opt == 's' || opt == 'S'){
					fflush(stdin);
					printf("Eh o MORCEGO!!!");
				}else if (opt == 'n' || opt == 'N'){
					fflush(stdin);
					printf("Eh Aquatico?: ");
					scanf("%c", &opt);
					if (opt == 's' || opt == 'S'){
						fflush(stdin);
						printf("Eh a BALEIA!!!");
					}
				}
			}
		}
	}else if (opt == 'n' || opt == 'N'){
	 //Cadeia de SELE��O das AVES.
	 	fflush(stdin);
	 	printf("� uma AVE?: ");
	 	scanf("%c", &opt);
	 	if (opt == 's' || opt == 'S'){
	 		fflush(stdin);
			printf("Eh N�o-Voadora?: ");
			scanf("%c", &opt);
			if (opt == 's' || opt == 'S'){
				fflush(stdin);
				printf("� Tropical?: ");
				scanf("%c", &opt);
				if (opt == 's' || opt == 'S'){
					fflush(stdin);
					printf("� o AVESTRUZ!!!");
				}else if (opt == 'n' || opt == 'N'){
					fflush(stdin);
					printf("� Polar?: ");
					scanf("%c", &opt);
					if (opt == 's' || opt == 'S'){
						fflush(stdin);
						printf("� o PINGUIM!!!");
					}
				}
			}else if (opt == 'n' || opt == 'N'){
				fflush(stdin);
				printf("� Nadador?: ");
				scanf("%c", &opt);
				if (opt == 's' || opt == 'S'){
					fflush(stdin);
					printf("� o PATO!!!");
				}else if (opt == 'n' || opt == 'N'){
					fflush(stdin);
					printf("� de Rapina!: ");
					scanf("%c", &opt);
					if (opt == 's' || opt == 'S'){
						fflush(stdin);
						printf("� A �GUIA!!!");
					}
				}
			}		
		}else if (opt == 'n' || opt == 'N'){
		//Cadeia de SELE��O dos R�PTILS.
			fflush(stdin);
			printf("� um R�ptil?: ");
			scanf("%c", &opt);
			if (opt == 's' || opt == 'S'){
				fflush(stdin);
				printf("� com Casco?: ");
				scanf("%c", &opt);
				if (opt == 's' || opt == 'S'){
					fflush(stdin);
					printf("� a TARTARUGA!!!");
				}else if (opt == 'n' || opt == 'N'){
					fflush(stdin);
					printf("� Carn�voro?: ");
					scanf("%c", &opt);
					if (opt == 's' || opt == 'S'){
						fflush(stdin);
						printf("� o Crocodilo!!!");
					}else if (opt == 'n' || opt == 'N'){
						fflush(stdin);
						printf("� sem Patas?: ");
						scanf("%c", &opt);
						if (opt == 's' || opt == 'S'){
							fflush(stdin);
							printf("� a COBRA!!!");
						}
					}
				}
			}
		}
	}
	return 0;
}
